package javafx.playground;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MainController implements Initializable {

	@FXML
	public ComboBox<String> comboBoxMathOperations;

	@FXML
	public Label labelOperationName;

	@FXML
	public Button buttonCalculate;

	@FXML
	public TextField textFieldOperationsHistory;

	@FXML
	public TextField textFieldNumberFirst;

	@FXML
	public TextField textFieldNumberSecond;

	private Double numberFirst;
	private Double numberSecond;

	private ObservableList<String> list = FXCollections.observableArrayList("Addition", "Subtraction", "Multiplication",
			"Division");

	public static boolean isNumeric(String numberString) {
		Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
		if (numberString == null) {
			return false;
		}
		return pattern.matcher(numberString).matches();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		comboBoxMathOperations.setItems(list);
		comboBoxMathOperations.setId("special-pane-id");
		textFieldOperationsHistory.setId("text-field-operation-history");
		textFieldOperationsHistory.setDisable(true);
		labelOperationName.setVisible(true);
		comboBoxMathOperations.getSelectionModel().selectFirst();
//		buttonCalculate.setId("button-calculate");
//		linking comboboxMathOperations with combobox in the fxml file
//		in the scenebuilder, go to the code section of combobox and paste in the fxid, the name of object from controller
	}

//	link this method with the combobox in the scene builder
//	go to scenebuilder, select combobox and choose in the Code section the method onAction
	public void comboBoxMathOperationsChanged(ActionEvent event) {
		labelOperationName.setText(comboBoxMathOperations.getValue());
	}

	public void calculateButtonClicked(ActionEvent event) {
		validateBothTextFields();
		String lineSeparator = System.lineSeparator();
		textFieldOperationsHistory.setText("RESULT: " + calculateResult(comboBoxMathOperations.getValue()) + lineSeparator);
	}

	public Double calculateResult(String operationString) {
		String command = operationString.toUpperCase();
		return switch (command) {
		case "ADDITION" -> numberFirst + numberSecond;
		case "SUBTRACTION" -> numberFirst - numberSecond;
		case "MULTIPLICATION" -> numberFirst * numberSecond;
		case "DIVISION" -> numberFirst / numberSecond;
		default -> 0.0;
			};
		
//		"Addition", "Subtraction", "Multiplication","Division"
	}

	public void validateBothTextFields() {
		String firstNumberString = textFieldNumberFirst.getText();
		String secondNumberString = textFieldNumberSecond.getText();
		if (!isNumeric(firstNumberString) || !isNumeric(secondNumberString)) {
			throw new IllegalArgumentException("One of the fields does not contain valid number!");
		}
		numberFirst = Double.parseDouble(firstNumberString);
		numberSecond = Double.parseDouble(secondNumberString);
	}

}
